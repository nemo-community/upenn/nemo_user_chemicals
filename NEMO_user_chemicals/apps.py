from django.apps import AppConfig


class NemoUserChemicalsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'NEMO_user_chemicals'
