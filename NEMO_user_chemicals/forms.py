from django.core.exceptions import ValidationError
from django.forms import ModelForm

from datetime import datetime

from django.forms import ModelForm
from django.utils import timezone

from NEMO_user_chemicals.models import (
	ChemicalRequest,
	UserChemical,
)

class ChemicalRequestApprovalForm(ModelForm):

	class Meta:
		model = ChemicalRequest
		fields = ['approved', 'approval_comments']

	def __init__(self, user, *args, **kwargs):
		super(ChemicalRequestApprovalForm, self).__init__(*args, **kwargs)
		self.user = user

	def save(self, commit=True):
		instance = super(ChemicalRequestApprovalForm, self).save(commit=False)
		instance.approver = self.user
		return super(ChemicalRequestApprovalForm, self).save(commit=commit)


class ChemicalRequestForm(ModelForm):

	class Meta:
		model = ChemicalRequest
		fields = ['requester', 'chemical_name', 'cas', 'container', 'sds_link', 'flammable', 'corrosive', 'reactive', 'temp_sensitive', 'stability', 'incompatibilities', 'health_hazards', 'exposure_routes', 'exposure_controls', 'procedure', 'hazardous_waste', 'waste_disposal']

	def __init__(self, user, *args, **kwargs):
		super(ChemicalRequestForm, self).__init__(*args, **kwargs)
		self.user = user

	def save(self, commit=True):
		instance = super(ChemicalRequestForm, self).save(commit=False)
		self.instance.requester = self.user
		if commit:
			instance.save()
		return super(ChemicalRequestForm, self).save(commit=commit)


class UserChemicalForm(ModelForm):

	class Meta:
		model = UserChemical
		fields = ['owner', 'label_id', 'in_date', 'expiration', 'chemical_name', 'sds_link', 'location']

	def clean_in_date(self):
		in_date = self.cleaned_data['in_date']
		return timezone.make_aware(datetime(year=in_date.year, month=in_date.month, day=in_date.day), timezone.get_current_timezone())

	def clean_expiration(self):
		expiration = self.cleaned_data['expiration']
		return timezone.make_aware(datetime(year=expiration.year, month=expiration.month, day=expiration.day), timezone.get_current_timezone())