from __future__ import annotations
import datetime
from django.core.exceptions import NON_FIELD_ERRORS, ValidationError

from NEMO.utilities import format_datetime
from NEMO.models import BaseModel, Consumable, ConsumableWithdraw, Project, User
from django.db import models
from django.utils import timezone


class ChemicalRequest(BaseModel):
	class Approval(object):
		PENDING = 0
		APPROVED = 1
		DENIED = 2
		Choices = (
			(PENDING, 'Pending'),
			(APPROVED, 'Approved'),
			(DENIED, 'Denied')
		)
	requester = models.ForeignKey(User, blank=True, null=True, related_name="chemical_requester", on_delete=models.SET_NULL)
	approver = models.ForeignKey(User, blank=True, null=True, related_name="chemical_approver", on_delete=models.SET_NULL)
	date = models.DateTimeField(auto_now_add=True)
	chemical_name = models.CharField(max_length=200)
	cas = models.CharField(max_length=100)
	container = models.CharField(max_length=100)
	sds_link = models.URLField(max_length=200)
	flammable = models.BooleanField()
	corrosive = models.BooleanField()
	reactive = models.BooleanField()
	temp_sensitive = models.BooleanField()
	stability = models.CharField(max_length=500)
	incompatibilities = models.CharField(max_length=500)
	health_hazards = models.CharField(max_length=500)
	exposure_routes = models.CharField(max_length=500)
	exposure_controls = models.CharField(max_length=500)
	procedure = models.TextField()
	hazardous_waste = models.BooleanField()
	waste_disposal = models.TextField()
	approved = models.IntegerField(choices=Approval.Choices, default=Approval.PENDING)
	approval_comments = models.TextField(blank=True, null=True)

	class Meta:
		ordering = ['-date']
		verbose_name_plural = "Chemical Requests"

	def __str__(self):
		return str(self.id)


class UserChemical(BaseModel):
	owner = models.ForeignKey(User, on_delete=models.PROTECT)
	label_id = models.PositiveIntegerField(unique=True)
	chemical_name = models.CharField(max_length=200)
	sds_link = models.URLField(max_length=200, blank=True, null=True)
	request = models.ForeignKey(ChemicalRequest, blank=True, null=True, on_delete=models.SET_NULL)
	in_date = models.DateField()
	expiration = models.DateField()
	location = models.CharField(max_length=100)

	class Meta:
		ordering = ['-expiration']

	def __str__(self):
		return str(self.label_id)

