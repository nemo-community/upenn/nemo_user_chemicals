from django import forms
from django.contrib import admin
from django.contrib.admin import register

from NEMO_user_chemicals.models import (
	ChemicalRequest, UserChemical
)


@register(UserChemical)
class UserChemicalAdmin(admin.ModelAdmin):
	list_display = ('owner', 'label_id', 'chemical_name', 'expiration')

@register(ChemicalRequest)
class ChemicalRequestAdmin(admin.ModelAdmin):
	list_display = ('id', 'requester', 'date', 'chemical_name', 'approved')