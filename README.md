# NEMO_User_Chemicals

Adapted from Princeton University's user chemicals feature.
Original code:  https://gitlab.com/mnfc-princeton/NEMO/-/blob/master/NEMO/views/user_chemicals.py

Setup instructions:
1) Install this plugin by running pip install --NEMO-user-chemicals
2) Add the user chemicals page as a landing page choice for all users, and the chemical inventory page as a landing page choice for staff.